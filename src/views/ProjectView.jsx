import React, { Component } from "react";
import {Grid, Row, Col, Table} from "react-bootstrap";
import { Card } from "components/Card/Card.jsx";
import { Tooltip, OverlayTrigger } from "react-bootstrap";
import { Tasks } from "components/Tasks/Tasks.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import { thMember, tdMember } from "variables/Variables.jsx";
import * as firebase from "firebase";
import SignIn from "./SignIn";


const db = firebase.firestore();

const Input = props => {
  return (
      <div>
        <input type="text" ref={props.inputRef} />
      </div>
  );
};

let text = [];
let currentText;

const createIssuePath = '/home/createissue';

let addTrigger = false;

const edit = <Tooltip id="edit_tooltip">Edit</Tooltip>;

class ProjectView extends Component {
  state = {
    value: '',
    savedValue: ''
  };

  addText = event =>
  {
    document.getElementById("textarea").disabled = false;
    document.getElementById("textarea").hidden = false;
    document.getElementById("savebutton").disabled = false;

    currentText = this.state.savedValue;
    addTrigger = true;

    this.setState({ value: this.state.textareaVal });
    this.state.textareaVal = "";
  };

  activateEdit = event =>
  {
    document.getElementById("textarea").disabled = false;
    document.getElementById("textarea").hidden = false;
    document.getElementById("savebutton").disabled = false;

    this.state.textareaVal = this.state.savedValue;
    currentText = this.state.savedValue;
    this.state.savedValue = "";
    this.setState({ value: this.state.textareaVal });
  };

  saveText = event =>
  {
    text = this.state.textareaVal;

    this.setState({ savedValue: text });
    document.getElementById("textarea").disabled = true;
    document.getElementById("textarea").hidden = true;
    document.getElementById("savebutton").disabled = true;

    if(addTrigger)
    {
      console.log(this.state.savedValue);
      this.state.savedValue += this.state.textareaVal;
      console.log(this.state.textareaVal);
      this.setState({ savedValue: this.state.savedValue});
      addTrigger = false;
    }
  };

  createLegend(json) {
    var legend = [];
    for (var i = 0; i < json["names"].length; i++) {
      var type = "fa fa-circle text-" + json["types"][i];
      legend.push(<i className={type} key={i} />);
      legend.push(" ");
      legend.push(json["names"][i]);
    }
    return legend;
  }

  render() {

    return (
        <div className="content">
          <Grid fluid>

            <Col md={7}>
              <Row md={8}>
                <Card rowsMax={20} onSubmit={this.submitText}
                      id="chartHours"
                      title="Description"
                      category="Project details"
                      content={
                        <div >
                          <tr>
                            <td>{this.state.savedValue}</td>
                            <OverlayTrigger placement="top" overlay={edit}>
                              <Button bsStyle="info" simple type="button" bsSize="md" onClick={this.activateEdit}>
                                <i className="fa fa-edit" />
                              </Button>
                            </OverlayTrigger>
                          </tr>
                          <textarea style={{width: '100%', height: '100%'}} id="textarea"
                                    value = {this.state.textareaVal}
                                    onChange={(event)=>{
                                      this.setState({
                                        textareaVal:event.target.value
                                      });
                                    }}
                          />
                          <Button style={{marginTop: 10, marginLeft:10, _height: 20, _weigh: 30, bsSizes:'large', fontSize: 13}} onClick={this.saveText} id="savebutton">
                            Save
                          </Button>
                          <Button style={{marginTop: 10, marginLeft:10, _height: 20, _weigh: 30, bsSizes:'large', fontSize: 13}} onClick={this.addText} id="addtext">
                            Add
                          </Button>
                        </div>
                      }
                />
              </Row>
              <Row md={6}>
                <Card
                    title="Issues"
                    category="Project issues"
                    content={
                      <div className="table-full-width">
                        <Button style={{marginTop: -100, marginLeft: 120}} onClick={() => {window.location.href = createIssuePath + "/"+ (window.location.href).slice((window.location.href).lastIndexOf('/') + 1)}}>
                          Add Issue
                        </Button>
                        <table className="table">
                          <Tasks />
                        </table>
                      </div>
                    }
                />
              </Row>

            </Col>

            <Col md={4} style={{marginLeft: 50}}>
              <Row md={4}>
                <Card
                    title="Members"
                    content={
                      <Table striped hover style={{marginTop: 2, marginLeft: -9}}>
                        <thead>
                        <tr>
                          {thMember.map((prop, key) => {
                            return <th key={key}>{prop}</th>;
                          })}
                        </tr>
                        </thead>
                        <tbody>
                        {tdMember.map((prop, key) => {
                          return (
                              <tr onClick={() => {console.log(prop[1])}}>
                                {prop.map((prop, key) => {
                                  return <td key={key}>{prop}</td>;
                                })}

                              </tr>
                          );
                        })}
                        </tbody>
                        <div>
                          <Button style={{marginTop: 10, marginLeft:10, _height: 20, _weigh: 50, bsSizes:'large', fontSize: 13}}>
                            Add
                          </Button>
                        </div>
                      </Table>

                    }
                />
              </Row>
            </Col>

            <Row>
            </Row>
          </Grid>
        </div>
    );
  }
}

export default ProjectView;
