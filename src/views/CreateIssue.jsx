import React, { Component } from "react";
import {
    Grid,
    Row,
    Col,
    FormGroup,
    ControlLabel,
    FormControl
} from "react-bootstrap";

import { Card } from "components/Card/Card.jsx";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

import NavDropdown from "react-bootstrap/lib/NavDropdown";
import MenuItem from "@material-ui/core/MenuItem";
import Nav from "react-bootstrap/lib/Nav";
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';

import { FilePond, File, registerPlugin } from 'react-filepond';
import firebase from 'firebase';
import 'filepond/dist/filepond.min.css';
import FilePondImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';

registerPlugin(FilePondImagePreview);

const db = firebase.firestore();
let uuidv4 = require('uuid/v4');
let uuid = uuidv4();

let selectedColour;
let selectedPriority;

const colours = [
    'Green', 'Yellow', 'Red'
];

const priorities = [
    'Low', 'Mid', 'High'
];

function getUid() {
    return uuid;
}

function getTitle () {
    return document.getElementById("title").value;
}

function getDescription () {
    return document.getElementById("description").value;
}

function getAffectedVersion () {
    return document.getElementById("affectedversion").value;
}

function getVersionToFix () {
    return document.getElementById("versiontofix").value;
}

function getColour() {
    return selectedColour;
}

function getPriority() {
    return selectedPriority;
}

function addIssueToDb(title, description, affected, fix, colour, priority) {
    setTimeout(function () {
        db.collection('Issues').doc(uuid).set({
            title: title,
            description: description,
            affected: affected,
            fix: fix,
            colour: colour,
            priority: priority,
        })
            .then(function(docRef) {
                console.log("Document written with ID: ", docRef.id);
            })
            .catch(function(error) {
                console.error("Error adding document: ", error);
            });
    }, 2000)
}

function getIssuedProjectId() {
    return new Promise((resolve, reject) => {
        setTimeout(function () {
            db.collection("Projects").where("name", "==", (window.location.href).slice((window.location.href).lastIndexOf('/') + 1))
                .get()
                .then(function (querySnapshot) {
                    querySnapshot.forEach(function (doc) {
                        // resolve(doc.data()["objectId"]);
                        resolve(doc.id);
                    });
                })
        }, 2000)

    })
}

function updateProjectInDbWithIssue(projectId) {
    setTimeout(function () {
        db.collection('Projects').doc(projectId).update({
            issues: firebase.firestore.FieldValue.arrayUnion(uuid),
        })
            .then(function(docRef) {
                console.log("Document written with ID: ", docRef.id);
            })
            .catch(function(error) {
                console.error("Error adding document: ", error);
            });
    }, 2000)
}

function emptyFields() {
    document.getElementById("title").value = null;
    document.getElementById("description").value = null;
    document.getElementById("affectedversion").value = null;
    document.getElementById("versiontofix").value = null;
}

function presentApp() {

    let path = '/home/user';
    window.location.href=path
    // console.log(firebase.auth().currentUser.uid);
}

function waitTime(title, description, affected, fix, colour, priority, projectUid) {

    setTimeout(function(){
        addIssueToDb(title, description, affected, fix, colour, priority);
    }, 2000);
    setTimeout(function(){
        updateProjectInDbWithIssue(projectUid);
    }, 2000);
    setTimeout(function(){
        emptyFields();
    }, 4000);
    setTimeout(function(){
        presentApp();
    }, 5000);

}

class CreateIssue extends Component {

    _onSelectColour = (option) => {
        selectedColour = option.label;
        console.log(selectedColour);
    };

    _onSelectPriority = (option) => {
        selectedPriority = option.label;
        console.log(selectedPriority);
    };

    constructor(props) {
        super(props);

        this.state = {
            files: [],
            uploadValue: 0,
            filesMetadata:[],
            rows:  [],
            message: "",
            picture: "",
            userProject: ""
        };

        this.handleProcessing = this.handleProcessing.bind(this);
    }

    componentDidMount() {
        getIssuedProjectId().then(res => {
            this.setState({userProject: res})
        })
    }

    handleInit() {
        // handle init file upload here
        console.log('now initialised', this.pond);
    }

    handleProcessing(fieldName, file, metadata, load, error, progress, abort) {
        // handle file upload here
        console.log(" handle file upload here");
        console.log(file);

        const fileUpload = file;
        const storageRef = firebase.storage().ref(`${getUid()}/${file.name}`);
        const storageRef2 = firebase.storage().ref();
        const task = storageRef.put(fileUpload);

        task.on(`state_changed` , (snapshot) => {
            console.log(snapshot.bytesTransferred, snapshot.totalBytes)
            let percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            //Process
            this.setState({
                uploadValue:percentage,
            })
        } , (error) => {
            //Error
            this.setState({
                message:`Upload error : ${error.message}`
            })
        } , () => {
            //Success
            this.setState({
                message: "Upload Success",
                picture: storageRef2.child(`${getUid()}/${file.name}`).getDownloadURL() //task.snapshot.downloadURL
            })
            console.log("Success!!!!!");
            this.pond.removeFile(); // remove file from view so you can know when it's uploaded
        })
    }

    render() {

        const {userProject} = this.state;

        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={8}>
                            <Card
                                title="Create Issue"
                                content={
                                    <form>
                                        <Row>
                                            <Col md={10}>
                                                <FormInputs
                                                    ncols={["col-md-8"]}
                                                    properties={[
                                                        {
                                                            label: "Title",
                                                            type: "text",
                                                            id: "title",
                                                            bsClass: "form-control",
                                                            placeholder: "Title",
                                                        }
                                                    ]}
                                                />
                                            </Col>
                                            <Col md={2}>
                                                <Dropdown options={colours} onChange={this._onSelectColour} placeholder="Colour" />
                                            </Col>
                                            <Col md={2}>
                                                <Dropdown options={priorities} onChange={this._onSelectPriority} placeholder="Priority" />
                                            </Col>
                                        </Row>
                                        <div className="form-group">
                                            <label htmlFor="exampleFormControlTextarea1">
                                                Project Description
                                            </label>
                                            <textarea
                                                className="form-control"
                                                id="description"
                                                rows="5"
                                                placeholder="Enter your description here..."
                                            />
                                        </div>
                                        <FormInputs
                                            ncols={["col-md-2", "col-md-2"]}
                                            properties={[
                                                {
                                                    label: "Affected",
                                                    type: "character",
                                                    id: "affectedversion",
                                                    bsClass: "form-control",
                                                    placeholder: "Version"
                                                },
                                                {
                                                    label: "To fix",
                                                    type: "character",
                                                    id: "versiontofix",
                                                    bsClass: "form-control",
                                                    placeholder: "Version"
                                                }
                                            ]}
                                        />

                                        <div className="Browse">
                                            <div className="Margin-25">
                                                <FilePond allowMultiple={true}
                                                          maxFiles={10}
                                                          ref= {ref => this.pond = ref}
                                                          server={{ process: this.handleProcessing.bind(this) }}
                                                          oninit={() => this.handleInit()}
                                                >

                                                    {this.state.files.map(file => (
                                                        <File key={file} source={file} />
                                                    ))}

                                                </FilePond>
                                            </div>
                                        </div>

                                        <Button bsStyle="info" pullRight fill
                                                onClick={() => waitTime(getTitle(), getDescription(), getAffectedVersion(), getVersionToFix(), getColour(), getPriority(), userProject)}>
                                            Create Issue
                                        </Button>

                                        <div className="clearfix" />
                                    </form>
                                }

                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default CreateIssue;
