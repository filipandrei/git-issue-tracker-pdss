import React from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import * as firebase from "firebase";

const useStyles = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const config = {
    apiKey: 'AIzaSyBPf_Gm3WRJ90cLFYw6rvjX6IpSADdh8V0',
    authDomain: 'git-issue-tracker.firebaseapp.com',
    databaseURL: 'https://git-issue-tracker.firebaseio.com',
    projectId: 'git-issue-tracker',
    storageBucket: 'git-issue-tracker.appspot.com',
    messagingSenderId: '472733831305',
    appId: '1:472733831305:web:6a3762e067271d2e79b658',
    measurementId: 'G-EN0ZCYRR28',
};

function resetPassword(email) {

    firebase.auth().sendPasswordResetEmail(email).then(r =>
        console.log("Successfully sent reset email!"));
}

function getEmail() {
    return document.getElementById("email").value;
}

export default function ForgotPassword() {

    const classes = useStyles();

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Typography component="h1" variant="h5">
                    Reset password
                </Typography>
                <div className={classes.form} noValidate>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="Email Address"
                        name="email"
                        autoComplete="email"
                        autoFocus
                    />
                    <Button onClick={() => resetPassword(getEmail())}
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                    >
                        Reset password
                    </Button>
                    <Grid container>
                        <Grid item xs>
                            <Link href="/signin" variant="body2">
                                Back to sign in
                            </Link>
                        </Grid>
                    </Grid>
                </div>
            </div>
        </Container>
    );
}


