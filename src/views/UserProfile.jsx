import React, { Component } from "react";
import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl
} from "react-bootstrap";

import { Card } from "components/Card/Card.jsx";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import { UserCard } from "components/UserCard/UserCard.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

import SignIn from "./SignIn";
import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

const db = firebase.firestore();
let projectsArray = [[]], projects;

function getEmail() {

  let data;

  setTimeout(function () {
    db.collection("Users").doc(firebase.auth().currentUser.uid).get().then(function(doc) {
      if (doc.exists) {
        data = doc.data();
        document.getElementById("email").value = data["email"];
      } else {
        console.log("No such document!");
        document.getElementById("email").value = ".";
      }
    })
  }, 1500)

}

function getFirstName() {

  let data;

  setTimeout(function () {
    db.collection("Users").doc(firebase.auth().currentUser.uid).get().then(function(doc) {
      if (doc.exists) {
        data = doc.data();
        document.getElementById("firstName").value = data["firstName"];
      } else {
        console.log("No such document!");
        document.getElementById("firstName").value = ".";
      }
    })
  }, 1500)

}

function getLastName() {

  let data;

  setTimeout(function () {
    db.collection("Users").doc(firebase.auth().currentUser.uid).get().then(function(doc) {
      if (doc.exists) {
        data = doc.data();
        document.getElementById("lastName").value = data["lastName"];
      } else {
        console.log("No such document!");
        document.getElementById("lastName").value = ".";
      }
    })
  }, 1500)

}

function getUsername() {

  let data;

  setTimeout(function () {
    db.collection("Users").doc(firebase.auth().currentUser.uid).get().then(function(doc) {
      if (doc.exists) {
        data = doc.data();
        document.getElementById("username").value = data["firstName"] + data["lastName"];
      } else {
        console.log("No such document!");
        document.getElementById("username").value = ".";
      }
    })
  }, 1500)

}

function updateUserDataInFirebase() {

  db.collection('Users').doc(firebase.auth().currentUser.uid).update({
    email: document.getElementById("email").value,
    firstName: document.getElementById("firstName").value,
    lastName: document.getElementById("lastName").value,
    username: document.getElementById("username").value,
  })
      .catch(function(error) {
        console.error("Error adding document: ", error);
      });
}

class UserProfile extends Component {

  render() {

    return (
        <div className="content">
          <Grid fluid>
            <Row>
              <Col md={8}>
                <Card
                    title="Edit Profile"
                    content={
                      <form>
                        <FormInputs
                            ncols={["col-md-6", "col-md-6"]}
                            properties={[
                              {
                                label: "Username",
                                type: "text",
                                id: "username",
                                bsClass: "form-control",
                                placeholder: "Username",
                                value: getUsername(),
                              },
                              {
                                label: "Email address",
                                type: "email",
                                id: "email",
                                bsClass: "form-control",
                                placeholder: "Email",
                                value: getEmail(),
                              }
                            ]}
                        />
                        <FormInputs
                            ncols={["col-md-6", "col-md-6"]}
                            properties={[
                              {
                                label: "First name",
                                type: "text",
                                id: "firstName",
                                bsClass: "form-control",
                                placeholder: "First name",
                                value: getFirstName(),
                              },
                              {
                                label: "Last name",
                                type: "text",
                                id: "lastName",
                                bsClass: "form-control",
                                placeholder: "Last name",
                                value: getLastName(),
                              }
                            ]}
                        />
                        <FormInputs
                            ncols={["col-md-14"]}
                            properties={[
                              {
                                label: "GitHub Token",
                                type: "character",
                                bsClass: "form-control",
                                placeholder: "Paste you GitHub Token here..."
                              }
                            ]}
                        />

                        <FormInputs
                            ncols={["col-md-14"]}
                            properties={[
                              {
                                label: "GitHub account",
                                type: "character",
                                bsClass: "form-control",
                                placeholder: "Place your GitHub user link..."
                              }
                            ]}
                        />
                        <Button onClick={() => updateUserDataInFirebase()}
                                style={{marginTop: 20, marginLeft: 20, _height: 30, _weigh: 40, bsSizes: 'large'}}
                                pullRight>
                          Update Profile
                        </Button>
                        <div className="clearfix"/>
                      </form>
                    }
                />
              </Col>
              <Col md={4}>
                <UserCard
                    name="Filip Andrei"
                    userName="filipandrei"
                    description={
                      <span>
                    "Project Manager/Normal User"
                  </span>
                    }
                    socials={
                      <div>
                        <Button simple onClick={() => {
                          window.open("https://github.com")
                        }}>
                          <i className="fa fa-github-square fa-lg"/>
                        </Button>
                      </div>
                    }
                />
              </Col>
            </Row>
          </Grid>
        </div>
    );
  }
}

export default UserProfile
