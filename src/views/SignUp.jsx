import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import SignIn from "./SignIn";
import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

// export let userData, userProjects;

const useStyles = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

// const config = {
//     apiKey: 'AIzaSyBPf_Gm3WRJ90cLFYw6rvjX6IpSADdh8V0',
//     authDomain: 'git-issue-tracker.firebaseapp.com',
//     databaseURL: 'https://git-issue-tracker.firebaseio.com',
//     projectId: 'git-issue-tracker',
//     storageBucket: 'git-issue-tracker.appspot.com',
//     messagingSenderId: '472733831305',
//     appId: '1:472733831305:web:6a3762e067271d2e79b658',
//     measurementId: 'G-EN0ZCYRR28',
// };
//
// firebase.initializeApp(config);
// firebase.firestore().enablePersistence().then(res => console.log(res));
const db = firebase.firestore();

function waitTime(email, password) {

    createUser(email, password);
}

function createUser(email, password) {

    if (email !== "" && password !== "" && document.getElementById("firstName").value !== "" &&
        document.getElementById("lastName").value !== "") {

        firebase.auth().createUserWithEmailAndPassword(String(email), String(password)).then(function(firebaseUser) {
            console.log("Created!");

            addUserToDb(email);

        });
        setTimeout(function(){
            firebase.auth().onAuthStateChanged(function(user) {
                if (user) {
                    let path = '/home/user';
                    window.location.href=path;
                } else {
                    let path = '/signup';
                    window.location.href=path;
                }
            });
        }, 4000);
    } else {
        document.getElementById("email").value = "";
        document.getElementById("password").value = "";
        document.getElementById("firstName").value = "";
        document.getElementById("lastName").value = "";
        let path = '/signup';
        window.location.href=path;
    }

}

function addUserToDb(email) {

    db.collection('Users').doc(firebase.auth().currentUser.uid).set({
        email: email,
        firstName: getFirstName(),
        lastName: getLastName()
    })
        .then(function(docRef) {
            console.log("Document written with ID: ", docRef.id);
        })
        .catch(function(error) {
            console.error("Error adding document: ", error);
        });

}

function getEmail () {
    return document.getElementById("email").value;
}

function getPassword () {
    return document.getElementById("password").value;
}

function getFirstName () {
    return document.getElementById("firstName").value;
}

function getLastName () {
    return document.getElementById("lastName").value;
}

function presentApp() {

    let path = '/home/user';
    window.location.href=path
    // console.log(firebase.auth().currentUser.uid);
}



function getUserProjects() {
    return new Promise( (resolve, reject) => {
        setTimeout(() => {
            db.collection("Users").doc(firebase.auth().currentUser.uid).get().then(function(doc) {
                if (doc.exists) {
                    resolve(doc.data()["projects"]);
                } else {
                    resolve({})
                }
            })
        }, 2000);
    })
}

class SignUp extends Component {

    render() {

        //const classes = useStyles();
        const classes = this.props;

        return (

            <Container component="main" maxWidth="xs">
                <CssBaseline/>
                <div className={classes.paper}>
                    <Typography component="h1" variant="h5">
                        Sign up
                    </Typography>
                    <div className={classes.form} noValidate>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    autoComplete="fname"
                                    name="firstName"
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="firstName"
                                    label="First Name"
                                    autoFocus
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="lastName"
                                    label="Last Name"
                                    name="lastName"
                                    autoComplete="lname"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="email"
                                    label="Email Address"
                                    name="email"
                                    autoComplete="email"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    name="password"
                                    label="Password"
                                    type="password"
                                    id="password"
                                    autoComplete="current-password"
                                />
                            </Grid>
                        </Grid>
                        <Button onClick={() => waitTime(getEmail(), getPassword())}
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                        >
                            Sign Up
                        </Button>
                        <Grid container justify="flex-end">
                            <Grid item>
                                <Link href="/signin" variant="body2">
                                    Already have an account? Sign in
                                </Link>
                            </Grid>
                        </Grid>
                    </div>
                </div>
            </Container>
        );
    }
}

export default SignUp;
